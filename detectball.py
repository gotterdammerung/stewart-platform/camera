#!/usr/bin/python

import argparse
import cv2
import imutils
import time
import socket

import numpy as np

from imutils.video import VideoStream

# args
parse = argparse.ArgumentParser()
parse.add_argument("-v", "--video", help="camera device")
parse.add_argument("-r", "--resolution", help="camera resolution")
args = vars(parse.parse_args())

# color range HSV
colorLower = (0, 0, 0)
colorUpper = (180, 255, 50)

# connect socket
IP = "127.0.0.1"
PORT = 60000

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))

# start video stream
if args["video"]:
	vs = VideoStream(args["video"]).start()
else:
	vs = VideoStream(src=0).start()

# resolution
if args["resolution"]:
	res = args["resolution"]
else:
	res = 500

res = int(res)
mid = int(res/2)

# start timer
time_prev = time.time()

while True:
	# get frame
	frame = vs.read()

	# resize and convert to the HSV
	frame = cv2.resize(frame, (res, res))
	blur = cv2.GaussianBlur(frame, (11, 11), 0)
	hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

	# remove blobs
	mask = cv2.inRange(hsv, colorLower, colorUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)

	# find contours in the mask
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	center = None

	if len(cnts) > 0:
		c = max(cnts, key=cv2.contourArea)
		((x, y), radius) = cv2.minEnclosingCircle(c)
		M = cv2.moments(c)
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
		# get coordinates from center
		cx = int(M["m10"] / M["m00"])
		cy = int(M["m01"] / M["m00"])
		# minimum size
		if radius > 10:
			# draw the circle
			cv2.circle(frame, (int(x), int(y)), int(radius),
				(0, 255, 255), 2)
			cv2.circle(frame, center, 5, (0, 0, 255), -1)
	else :
		cx = int(res/2)
		cy = int(res/2)

	# center and axis
	cv2.circle(frame, (mid, mid), 5, (0, 0, 0), -1)
	cv2.line(frame, (0, mid), (res, mid), (100, 0, 0), 1)
	cv2.line(frame, (mid, 0), (mid, res), (100, 0, 0), 1)

	# normalization
	cx = int(100*cx/res)
	cy = int(100*cy/res)

	# send by socket
	mess = f'({cx:03d},{cy:03d})'
	print(mess)
	s.send(bytes(mess, 'utf-8'))

	# timer
	print(f"fps: {int(1/(time.time()-time_prev))}")
	time_prev = time.time()

	# show camera
	cv2.imshow("Frame", frame)

	key = cv2.waitKey(1) & 0xFF
	# break loop
	if key == ord("q"):
		break
# end
vs.stop()
cv2.destroyAllWindows()
s.close()
